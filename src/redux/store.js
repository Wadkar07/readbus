import { configureStore } from "@reduxjs/toolkit";
import bus from "./reducer/bus";

export default configureStore({
    reducer : {
        bus
    }
})