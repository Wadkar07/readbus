import { FILTER_BUSES, INITIAL,ERROR } from "./action";
const initalState = {
    from:'',
    to:'',
    busList: [],
    filter: [],
    error: ''
}

export default function bus(state = initalState, action) {
    switch (action.type) {
        case INITIAL:{
            return {
                ...state,
                busList: action.payload.busData,
                filter: action.payload.busData,
                from:action.payload.from? action.payload.from : state.from,
                to: action.payload.to?action.payload.to: state.to
            }
        }        
        case FILTER_BUSES: {
            let deafultList = state.busList;
            let filteredBusList = deafultList.filter((bus) => {
                return (action.payload.departureTime).includes(bus["departureTime"]);
            })
            
            if (action.payload.departureTime.length) {

                if ((action.payload.busType.length)) {
                    let filteredBusListByType = state.busList.filter((bus) => {
                        return (action.payload.busType).includes(bus["typeOfBus"]);
                    })
                    return {
                        ...state,
                        filter: filteredBusListByType
                    }
                }
                else {
                    return {
                        ...state,
                        filter: filteredBusList
                    }
                }
            }
            else if ((action.payload.busType.length)) {
                let filteredBusListByType = state.filter.filter((bus) => {
                    return (action.payload.busType).includes(bus["typeOfBus"]);
                })
                return {
                    ...state,
                    filter: filteredBusListByType
                }
            }
            else if ((action.payload.arrivalTime.length)) {
                let filteredBusListByArrival = state.busList.filter((bus) => {
                    return (action.payload.arrivalTime).includes(bus["arrivalTime"]);
                })
                return {
                    ...state,
                    filter: filteredBusListByArrival
                }
            }
            else {
                return {
                    ...state,
                    filter:deafultList
                };
            }
            
        }
        case ERROR:{
            return {
                ...state,
                error:action.payload
            }
        }
        default: return state;
    }
}