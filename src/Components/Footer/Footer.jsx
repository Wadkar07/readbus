import React, { Component } from 'react'
import './Footer.css'
import logo from '../../images/white-red-bus-logo.png'

export default class Footer extends Component {
  render() {
    return (
      <div className='footer'>
        <div className="footer-section">
          <div className="footer-left">
            <div className='footer-about-redbus footer-div'>
              <h3>About redBus</h3>
              <p>About Us</p>
              <p>Contact Us</p>
              <p>Mobile Version</p>
              <p>redBus on Mobile</p>
              <p>Sitemap</p>
              <p>Offers</p>
              <p>Careers</p>
              <p>Values</p>
            </div>
            <div className='footer-info footer-div'>
              <h3>Info</h3>
              <p>T & C</p>
              <p>Privacy Policy</p>
              <p>FAQ</p>
              <p>Blog</p>
              <p>Bus Operator Registration</p>
              <p>Agent Registration</p>
              <p>Insurance Partner</p>
              <p>User Agreement</p>
            </div>
            <div className='footer-global-sites footer-div'>
              <h3>Global Sites</h3>
              <p>India</p>
              <p>Singapore</p>
              <p>Malaysia</p>
              <p>Indonesia</p>
              <p>Peru</p>
              <p>Colombia</p>
            </div>
            <div className='footer-our-partners footer-div'>
              <h3>Our Partners</h3>
              <p>Goibibo</p>
              <p>Makemytrip</p>
              <h3>Other Services</h3>
              <p>Train Tickets</p>
              <p>Bus Hire</p>
              <p>Cab Booking</p>
              <p>Tempo Traveller</p>
            </div>
          </div>
          <div className='footer-redbus footer-div'>
            <img src={logo} alt="redbus" />
            <p>
              redBus is the world's largest <strong>online bus ticket booking </strong> service trusted by over 25 million happy customers globally. redBus offers bus ticket booking through its website,iOS and Android mobile apps for all major routes.
            </p>
            <div>
              <i className="fa-brands fa-facebook-f"></i>
              <i className="fa-brands fa-twitter"></i>
            </div>
            <p>
              Ⓒ 2023 ibibogroup All rights reserved
            </p>
          </div>
        </div>
      </div>
    )
  }
}
