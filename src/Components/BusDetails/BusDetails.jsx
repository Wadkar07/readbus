import React, { Component } from 'react'

export default class BusDetails extends Component {
    render() {
        const { traveler, busType, departure, boardingPoint, duration, arrival, date, droppingPoint, rating, ratingCount, price } = this.props.busInfo;
        const seatsAvailable = this.props.seatsAvailable;
        return (
            <div className="bus">
                <div className='bus-type'>
                    <strong>{traveler}</strong>
                    <small>{busType}</small>
                </div>
                <div className='departure'>
                    <h2>{departure}</h2>
                    <small>{boardingPoint}</small>
                </div>
                <div className='duration'>
                    <p>{duration}</p>
                </div>
                <div className='arrival'>
                    <p>{arrival}</p>
                    <small>{date}</small>
                    <small>{droppingPoint}</small>
                </div>
                <div className='rating'>
                    <small className='rate'><i className="fa-solid fa-star"></i> {rating}</small>
                    {ratingCount ? <small className='rating-count'><i className="fa-solid fa-users"></i>{ratingCount}</small> : <></>}
                </div>
                <div className='price'>
                    <strong>₹ {price}</strong>
                </div>
                <div className='seats'>
                    <small>{seatsAvailable} Seats Available</small><br />
                </div>
            </div>
        )
    }
}
