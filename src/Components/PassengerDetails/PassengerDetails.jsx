import React, { Component } from 'react'

export default class PassengerDetails extends Component {
    render() {
        const {seat,index, error,checkPassengerDetails} = this.props;
        return (
            <div className='m-3 p-3 border shadow'>
                <form className="row g-3">
                    <label forHtml="inputAddress">
                        Passenger {index + 1} | <strong>{seat}</strong>
                    </label>
                    <div className="form-row text-left">
                        <label forHtml={`name${index}`} className="form-label">Name</label>
                        <input type="text" className="form-control" name={`name${index}`} id={`name${index}`} placeholder="Name" onChange={(event) => {
                            checkPassengerDetails(event, seat);
                        }} />
                        <small className='text-danger'>{error[`name${index}`]}</small>
                    </div>

                    <div className="form-row d-flex justify-content-between text-left">
                        <div>
                            <div >
                                <label className="form-label">Gender</label>
                            </div>
                            <div className='d-flex'>
                                <div>
                                    <div className="form-check-inline text-left">
                                        <input className="form-check-input " type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" defaultChecked />
                                        <label className="form-check-label " forHtml="inlineRadio1">Male</label>
                                    </div>
                                </div>
                                <div className="form-check-inline text-left">
                                    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
                                    <label className="form-check-label" forHtml="inlineRadio2">Female</label>
                                </div>
                            </div>
                        </div>
                        <div className="form-check-inline">
                            <label forHtml={`age${index}`} className="form-label">Age</label>
                            <input type="Number" className="form-control" name={`age${index}`} id={`age${index}`} onChange={(event) => {
                                checkPassengerDetails(event);
                            }} />
                            <small className='text-danger'>{error[`age${index}`]}</small>
                        </div>
                    </div>

                    <div className="form-row">
                        <label forHtml={`city${index}`} className="form-label">City of Residence</label>
                        <input type="text" className="form-control" name={`city${index}`} id={`city${index}`} onChange={(event) => {
                            checkPassengerDetails(event);
                        }} />
                        <small className='text-danger'>{error[`city${index}`]}</small>
                    </div>
                    <div className="form-row">
                        <label forHtml={`state${index}`} className="form-label">state</label>
                        <input type="text" className="form-control" name={`state${index}`} id={`state${index}`} onChange={(event) => {
                            checkPassengerDetails(event);
                        }} />
                        <small className='text-danger'>{error[`state${index}`]}</small>
                    </div>
                </form>
            </div>
        )
    }
}
