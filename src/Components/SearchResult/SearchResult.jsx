import React, { Component } from 'react'
import './SearchResult.css'
import Bus from '../Bus/Bus';
import Filter from '../Filter/Filter';
import { connect } from 'react-redux'
import Loader from '../Loader/Loader';
import axios from 'axios';
import { INITIAL } from '../../redux/reducer/action';

class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.API_STATES = {
      LOADING: "LOADING",
      LOADED: "LOADED",
      ERROR: "ERROR"
    }

    this.URL = "https://redbus-api.vercel.app/api/buses";

    this.state = {
      buses: [],
      status: this.API_STATES.LOADING,
      errorMessage: "",

    };
  }

  fetchBuses = (url) => {
    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {

      axios.get(url)
        .then((response) => {
          this.setState({
            status: this.API_STATES.LOADED,
            buses: response.data,
          }, () => {
            this.props.getBuses({ busData: this.state.buses })
          })

        })
        .catch((error) => {
          this.setState({
            status: this.API_STATES.ERROR,
            errorMessage: "An API error occurred. Please try again in a few minutes."
          })
        })
    })
  }

  componentDidMount() {
    this.fetchBuses(this.URL)
  }

  reloadData = () => {
    this.fetchBuses(this.URL)
  }
  render() {
    const from = localStorage.getItem('from');
    const to = localStorage.getItem('to');
    const date = '30 April';
    const day = 'Sun';
    return (
      <>
        {this.state.status === 'LOADING' && <Loader />}

        {this.state.errorMessage &&
          <div style={{ height: "46.7vh" }} className='d-flex justify-content-center align-items-center flex-column'>
            <h1 className='text-danger'>
              <strong>{this.state.error}</strong>
            </h1>
          </div>
        }

        {this.state.status === this.API_STATES.LOADED && this.state.buses.length > 0 && <div className='search-result'>
          <div className="journey-tab">
            <strong>{from} <i className="fa-solid fa-arrow-right"></i> {to}</strong>
            <strong>
              <div className="date-div">

                <div className='date-day'>
                  <small><i className="fa-solid fa-chevron-left"></i> {date} <i className="fa-solid fa-chevron-right"></i></small>
                  <small>{day}</small>
                </div>

              </div>
            </strong>
            <button> Modify</button>
          </div>
          <div className="filter-buses">
            <div className="filter">
              <Filter />
            </div>

            <div className="buslist">
              {
                (
                  this.props.bus?.map((bus) => {
                    return <Bus key={bus.id} bus={bus} />
                  }))
              }
            </div>
          </div>
        </div>}
      </>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getBuses: (payload) => dispatch(
      {
        type: INITIAL,
        payload
      }
    )
  }

}
function mapStateToProps(state) {
  return {
    bus: state.bus.filter,
    from: state.bus.from,
    to: state.bus.to,
    error: state.bus.error
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchResult);
