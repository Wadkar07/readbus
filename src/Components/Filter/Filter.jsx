import React, { Component } from 'react'
import morning from '../../images/morning.png'
import noon from '../../images/weather.png'
import evening from '../../images/sunset-.png'
import night from '../../images/sea.png'
import { FILTER_BUSES } from '../../redux/reducer/action';
import { connect } from 'react-redux';

import './Filter.css'

class Filter extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchFilter: {
                departureTime: [],
                busType: [],
                arrivalTime: []
            }
        }
    }
    handleClick = (event) => {
        const name = event.target.name;

        if (name.includes('dep')) {
            if (!(this.state.searchFilter.departureTime).includes(name))
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        searchFilter: {
                            ...prevState.searchFilter,
                            departureTime: [...prevState.searchFilter.departureTime, name]
                        }
                    }
                }, () => {
                    this.props.filterBuses(this.state.searchFilter)
                })
            else {
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        searchFilter: {
                            ...prevState.searchFilter,
                            departureTime: prevState.searchFilter.departureTime.filter((category) => {
                                return category !== name;
                            })
                        }
                    }
                }, () => {
                    this.props.filterBuses(this.state.searchFilter)
                })
            }
        } else if (name.includes('type')) {
            if (!(this.state.searchFilter.busType).includes(name))
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        searchFilter: {
                            ...prevState.searchFilter,
                            busType: [...prevState.searchFilter.busType, name]
                        }
                    }
                }, () => {
                    this.props.filterBuses(this.state.searchFilter);
                })
            else {
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        searchFilter: {
                            ...prevState.searchFilter,
                            busType: prevState.searchFilter.busType.filter((category) => {
                                return category !== name;
                            })
                        }
                    }
                }, () => {
                    this.props.filterBuses(this.state.searchFilter);
                })
            }
        }
        else {
            if (!(this.state.searchFilter.arrivalTime).includes(name))
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        searchFilter: {
                            ...prevState.searchFilter,
                            arrivalTime: [...prevState.searchFilter.arrivalTime, name]
                        }
                    }
                }, () => {
                    this.props.filterBuses(this.state.searchFilter);
                })
            else {
                this.setState((prevState) => {
                    return {
                        ...prevState,
                        searchFilter: {
                            ...prevState.searchFilter,
                            arrivalTime: prevState.searchFilter.arrivalTime.filter((category) => {
                                return category !== name;
                            })
                        }
                    }
                }, () => {
                    this.props.filterBuses(this.state.searchFilter);
                })
            }
        }
    }
    render() {
        return (
            <div className='filter-div'>
                <strong>Filter</strong>
                <div className="departure-time">
                    <strong>DEPARTURE TIME</strong><br />

                    <input type="checkbox" className='check-box form-check-input' id="dep_before_6_AM" name="dep_before_6_AM" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label htmlFor="dep_before_6_AM">
                        <img src={morning} className="icon" alt="morning" />
                        Before 6 am
                    </label><br />

                    <input type="checkbox" className='check-box form-check-input' id="dep_from_6_to_12" name="dep_from_6_to_12" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label htmlFor="dep_from_6_to_12">
                        <img src={noon} className="icon" alt="morning" />
                        6 am to 12 pm
                    </label><br />

                    <input type="checkbox" className='check-box form-check-input' id="dep_from_12_to_6" name="dep_from_12_to_6" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label htmlFor="dep_from_12_to_6">
                        <img src={evening} className="icon" alt="morning" />
                        12 pm to 6 pm
                    </label><br />

                    <input type="checkbox" className='check-box form-check-input' id="dep_after_6" name="dep_after_6" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label htmlFor="dep_after_6" >
                        <img src={night} className="icon" alt="morning" />
                        After 6 pm
                    </label><br />
                </div>
                <div className="departure-time">
                    <strong>BUS TYPES</strong><br />

                    <input type="checkbox" className='check-box form-check-input' name="type_sleeper" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label>SLEEPER </label><br />

                    <input type="checkbox" className='check-box form-check-input' name="type_AC" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label>AC </label><br />

                    <input type="checkbox" className='check-box form-check-input' name="type_NON_AC" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label>NONAC </label><br />
                </div>
                <div className="departure-time">
                    <strong>SEAT AVAILABILITY</strong><br />
                    <input type="checkbox" className='check-box form-check-input' name="seat_availability" onClick={(event) => {
                        this.handleClick(event);
                    }} /><label>Single Seats </label><br />
                </div>
                <div className="departure-time">
                    <strong>ARRIVAL TIME</strong><br />

                    <input type="checkbox" className='check-box form-check-input' id="arv_before_6_AM" name="arv_before_6_AM" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label htmlFor='arv_before_6_AM'>
                        <img src={morning} className="icon" alt="morning" />
                        Before 6 am
                    </label><br />

                    <input type="checkbox" className='check-box form-check-input' id="arv_from_6_to_12" name="arv_from_6_to_12" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label htmlFor='arv_from_6_to_12'>
                        <img src={noon} className="icon" alt="morning" />
                        6 am to 12 pm
                    </label><br />

                    <input type="checkbox" className='check-box form-check-input' id="arv_from_12_to_6" name="arv_from_12_to_6" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label htmlFor='arv_from_12_to_6'>
                        <img src={evening} className="icon" alt="morning" />
                        12 pm to 6 pm
                    </label><br />

                    <input type="checkbox" className='check-box form-check-input' id="arv_after_6" name="arv_after_6" onClick={(event) => {
                        this.handleClick(event);
                    }} />
                    <label htmlFor='arv_after_6'>
                        <img src={night} className="icon" alt="morning" />
                        After 6 pm
                    </label><br />

                </div>

                <div>
                    <strong>Boarding Point</strong>
                    <input type="text" />
                </div>

                <div>
                    <strong>Dropping Point</strong>
                    <input type="text" />
                </div>

                <div>
                    <strong>Operator</strong>
                    <input type="text" />
                </div>

                <div>
                    <strong>AMENITIES</strong>
                    <button>WIFI (10)</button>
                    <button>Water Bottle (14)</button>
                    <button>Blankets (30)</button>
                    <button>Charging Point (43)</button>
                    <button>Movie (10)</button>
                    <button>Track My Bus (40)</button>
                    <button>Emergency Contact Nu... (42)</button>
                    <button>Toilet (5)</button>
                    <button>Bed Sheet (15)</button>
                </div>

            </div>
        )
    }
}

const mapDispatchToProps = {
    filterBuses: (payload) => {
        return {
            type: FILTER_BUSES,
            payload
        }
    }
}
export default connect(null, mapDispatchToProps)(Filter)