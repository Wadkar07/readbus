import React, { Component } from 'react'
import './Award.css'
import businessStandard from '../../images/Business_Standard1.png'
import trustedBrand from '../../images/Brand_Trust_Report.png'
import innovation from '../../images/Eye_for_Travel1.png'

export default class Award extends Component {
  render() {
    return (
      <div className='award'>
        <h2>AWARDS & RECOGNITION</h2>
        <div className="recognition">
            <div className='recognition1'>
                <img src={businessStandard} alt="" />
                <small>Most Innovative Company</small>
            </div>
            <div className='recognition2'>
                <img src={trustedBrand} alt="" />
                <small>Most Trudted Brand </small>
            </div>
            <div className='recognition3'>
                <img src={innovation} alt="" />
                <small>Mobile Innovation Award</small>
            </div>
        </div>
      </div>
    )
  }
}
