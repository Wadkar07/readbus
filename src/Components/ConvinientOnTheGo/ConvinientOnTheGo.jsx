import React, { Component } from 'react'
import mobileImage from '../../images/IOS_Android_device.png'
import './ConvinientOnTheGo.css'

export default class ConvinientOnTheGo extends Component {
    render() {
        return (
            <div className='convinient-on-the-go'>
                <div className='otg-content'>
                    <h1>CONVENIENCE ON-THE-GO.</h1>
                    <div>
                        Enjoy the following exclusive features on the redBus app
                    </div>
                    <div>
                        Last Minute Booking - In a hurry to book a bus at the last minute? Choose the bus passing from your nearest boarding point and book in a few easy steps.
                    </div>
                    <div>
                        Boarding Point Navigation - Never lose your way while travelling to your boarding point!
                    </div>
                    <div>
                        Comprehensive Ticket Details- Everything that you need to make the travel hassle free - rest stop details, boarding point images, chat with co-passengers, wake-up alarm and much more!
                    </div>
                    <div className='enter-phone-number'>
                        Enter your mobile number below to download the redBus mobile app.
                        <div className="phone-no">
                            <select name=""></select>
                            <input type="number" name="phone" />
                        </div>
                        <button>SMS ME THE LINK</button>
                    </div>
                </div>
                <div className="img-div">
                    <img src={mobileImage} alt="" />
                </div>
            </div>
        )
    }
}
