import React, { Component } from 'react'
import unavailableSleeper from '../../images/unavailableSleeper.png'
import driver from '../../images/steering-wheel.png'
import sleeper from '../../images/sleeper.png'
import checkedSeat from '../../images/checked.png'

export default class Seats extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: true,
            defaultSeatsLower: [],
            bookedSeatsLower: [],
            defaultSeatsUpper: [],
            bookedSeatsUpper: [],
            booking: [],
            id: '',
            price: 0
        }
    }
    componentDidMount() {
        this.setState({
            defaultSeatsLower: this.props.seatBookedForLowerPlatform,
            bookedSeatsLower: this.props.seatBookedForLowerPlatform,
            defaultSeatsUpper: this.props.seatBookedForUpperPlatform,
            bookedSeatsUpper: this.props.seatBookedForUpperPlatform,
            price: this.props.price,
            id: this.props.id,
        })
    }
    bookSeat = (indexBooked, floor) => {



        const floorTag = floor === "bookedSeatsLower" ? 'L' : 'U';
        this.setState((prevState) => {
            return {
                ...prevState,
                [floor]: this.state[floor].map((seat, index) => {
                    if (`${floorTag}${index}` === indexBooked) {
                        if (seat !== 1) {
                            return 1;
                        }
                        else {
                            if (floorTag === 'L') {
                                return this.state.defaultSeatsLower[index];
                            }
                            else if (floorTag === 'U') {
                                return this.state.defaultSeatsUpper[index];
                            }
                            else {
                                return null;
                            }
                        }
                    }
                    else {
                        return seat;
                    }
                }),
            }
        })
        if ((this.state.booking).includes(indexBooked)) {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    booking: prevState.booking.filter((seatNumber) => {
                        return seatNumber !== indexBooked;
                    })
                }
            })
        }
        else {
            indexBooked = floor === "bookedSeatsUpper" ? `U${indexBooked}` : `L${indexBooked}`
            if (!this.state.booking.includes(indexBooked)) {
                this.setState({
                    booking: [...this.state.booking, indexBooked]
                }, () => {
                    const { id, booking, price, bookedSeatsLower, bookedSeatsUpper } = this.state;
                    this.props.showBill(id, price, bookedSeatsLower, bookedSeatsUpper, booking);
                })
            }
            else {
                this.setState((prevState) => {
                    return {
                        booking: prevState.booking.filter((seat) => {
                            return seat !== indexBooked;
                        })
                    }
                }, () => {
                    const { id, booking, price, bookedSeatsLower, bookedSeatsUpper } = this.state;
                    this.props.showBill(id, price, bookedSeatsLower, bookedSeatsUpper, booking);
                })
            }
        }


    }
    render() {
        return (
            <div style={{ background: "#EEEDED" }}>
                <div className='lower-platform p-3'>
                    <div style={{ borderLeft: ".5rem solid gray", background: "#FFFFFF" }} className=" d-flex align-middle lower py-2">
                        <div>
                            <img src={driver} style={{ height: "2rem" }} className='m-2' alt="" />
                        </div>
                        <div style={{ width: '100%', height: '9rem' }} className='d-flex flex-coloumn flex-wrap align-middle ' >
                            {this.state.bookedSeatsLower.map((seat, index) => {
                                return (
                                    <div style={{ height: "33%" }}>
                                        {seat ?
                                            seat === 1 ?
                                                <img src={checkedSeat} style={{ width: "3rem" }} key={`L${index}`} className="mx-2" alt="index" onClick={() => {
                                                    this.bookSeat(index, "bookedSeatsLower")
                                                }} /> :
                                                <img src={unavailableSleeper} style={{ width: "3rem" }} key={`L${index}`} className="mx-2" alt="index" onClick={() => {

                                                }} />

                                            :
                                            (<img src={sleeper} style={{ width: "3rem", borderRadius: ".2rem" }} key={`LB${index}`} className="mx-2" alt="index" onClick={() => {
                                                this.bookSeat(index, "bookedSeatsLower")
                                            }} />)
                                        }
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
                <div className="upper-platform p-3">
                    <div style={{ borderLeft: ".5rem solid gray", background: "#FFFFFF" }} className="d-flex lower py-3">
                        <img src={driver} style={{ height: "2rem" }} className='m-2' alt="" />
                        <div style={{ width: '100%', height: '9rem' }} className='d-flex flex-coloumn flex-wrap' >
                            {this.state.bookedSeatsUpper.map((seat, index) => {
                                return (
                                    <div style={{ height: "33%" }}>
                                        {seat ?
                                            (<img src={checkedSeat} style={{ width: "3rem" }} key={`U${index}`} className="mx-2" alt="index" onClick={() => {
                                                this.bookSeat(index, "bookedSeatsUpper")
                                            }} />)
                                            :
                                            (<img src={sleeper} style={{ width: "3rem", borderRadius: ".2rem" }} key={`UB${index}`} className="mx-2" alt="index" onClick={() => {
                                                this.bookSeat(index, "bookedSeatsUpper")
                                            }} />)
                                        }
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
