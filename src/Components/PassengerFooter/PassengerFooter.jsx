import React, { Component } from 'react'

export default class PassengerFooter extends Component {
    render() {
        const {bill,errors,errorFields} = this.props;
        return (
            <div style={{ bottom: "0", position: "fixed", background: "white", padding: "1rem", width: "35rem" }}>
                <small style={{ fontSize: "0.65rem", margin: "2rem 0" }}>By clicking on proceed, I agree that I have read and understood the TnCs and the Privacy Policy</small>
                <div className='d-flex justify-content-between'>
                    <div>
                        <strong>Total Amount : INR {bill}</strong>
                    </div>
                    {
                        errors.length ?
                            <button type="button" className="btn btn-danger" href={"#" + errorFields[0]}>
                                Proceed to Pay
                            </button>
                            :
                            <button type="button" className="btn btn-danger" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                Proceed to Pay
                            </button>
                    }
                </div>
            </div>
        )
    }
}
