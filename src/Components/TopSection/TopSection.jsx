import React, { Component } from 'react'
import './TopSection.css'
import { operators } from '../../data/operators'

export default class TopSection extends Component {
    render() {
        return (
            <div className='top-section'>
                <div className="top-row-1 row-1">
                    <div className='row1-div'>
                        <strong>Top Bus Routes</strong>
                        <small>Hyderabad to Bangalore Bus</small>
                        <small>Bangalore to Chennai Bus</small>
                        <small>Pune to Bangalore Bus</small>
                        <small>Mumbai to Bangalore Bus</small>
                        <small>More </small>
                    </div>
                    <div className='row1-div'>
                        <strong>Top Cities</strong>
                        <small>Hyderabad Bus Tickets</small>
                        <small>Bangalore Bus Tickets</small>
                        <small>Chennai Bus Tickets</small>
                        <small>Pune Bus Tickets</small>
                        <small>More </small>
                    </div>
                    <div className='row1-div'>
                        <strong>Top RTC's</strong>
                        <small>APSRTC</small>
                        <small>GSRTC</small>
                        <small>MSRTC</small>
                        <small>TNSTC</small>
                        <small>More </small>
                    </div>
                    <div className='row1-div'>
                        <strong>Other</strong>
                        <small>TSRTC</small>
                        <small>SBSTC</small>
                        <small>RSRTC</small>
                        <small>KeralaRTC</small>
                        <small>More </small>
                    </div>
                    <div className='row1-div'>
                        <strong>Tempo Traveller in Cities</strong>
                        <small>Tempo traveller in Bangalore</small>
                        <small>Tempo traveller in Chennai</small>
                        <small>Tempo traveller in Mumbai</small>
                        <small>Tempo traveller in Hyderabad</small>
                        <small>Tempo traveller in Ahmedabad</small>
                    </div>
                </div>
                <div className="top-row-1">
                    <div className="operators-div">
                        <h4>Top Operators</h4>
                        <div className="operators-list">
                            {
                                operators.map((operator,index)=>{
                                    if(index ===0){
                                        return <span key={index} className="operator-1">{operator}</span>
                                    }
                                    else{
                                        return <span key={index} className='operator'>{operator}</span>
                                    }
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
