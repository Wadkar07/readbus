import React, { Component } from 'react'
import './GrowingNumber.css'

export default class GrowingNumber extends Component {
    render() {
        return (
            <div className='growing-number'>
                <h2>THE NUMBERS ARE GROWING!</h2>
                <div className="growing-sectors">
                    <div>
                        <small>CUSTOMERS</small>
                        <h1>36M</h1>
                        <small>redBus is trusted by over 36 million happy customers globally</small>
                    </div>
                    <div>
                        <small>OPERATORS</small>
                        <h1>3500</h1>
                        <small>network of over 3500 bus operators worldwide</small>
                    </div>
                    <div>
                        <small>BUS TICKETS</small>
                        <h1>220+ M</h1>
                        <small>Over 220 million trips booked using redBus</small>
                    </div>
                </div>
            </div>
        )
    }
}
