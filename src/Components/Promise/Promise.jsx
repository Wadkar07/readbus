import React, { Component } from 'react'
import './Promise.css'
import promise from '../../images/promise.png'
import benifits from '../../images/benefits.png'
import customerCare from '../../images/customer_care.png'
import fare from '../../images/lowest_Fare.png'

export default class Promise extends Component {
  render() {
    return (
      <div className='promise'>
        <div className="image-cap">
            <img src={promise} alt="promise" />
            <h1>WE PROMISE TO DELIVER</h1>
        </div>
        <div className="promise-divs">
            <div className="benifits">
                <img src={benifits} alt="" />
                <p className="cap">UNMATCHED BENEFITS</p>
                <small className="description">We take care of your travel beyond ticketing by providing you with innovative and unique benefits.</small>
            </div>
            <div className="customer-care">
                <img src={customerCare} alt="" />
                <p className="cap">SUPERIOR CUSTOMER SERVICE</p>
                <small className="description">We put our experience and relationships to good use and are available to solve your travel issues.</small>
            </div>
            <div className="fare">
                <img src={fare} alt="" />
                <p className="cap">LOWEST PRICES</p>
                <small className="description">We always give you the lowest price with the best partner offers.</small>
            </div>
        </div>
      </div>
    )
  }
}
