import React, { Component } from 'react'
import firstSave from '../../images/first.png'
import './FirstSave.css'

export default class FirstSave extends Component {
  render() {
    return (
        <div className="first-save">
        <img src={firstSave} alt="first-save" />
        <div>
            <h2>SAVE UPTO RS 250* ON YOUR BUS TICKETS.</h2>
            <small>Book your favourite seat now.</small>
        </div>
    </div>
    )
  }
}
