import React, { Component } from 'react'

export default class ContactDetails extends Component {
    render() {
        return (
            <>
                <p className='text-left border-top border-bottom  w-100 p-2'>
                    <strong><i className="fa-regular fa-envelope"></i> Contact Details</strong>
                </p>
                <form className=" w-100 ms-3 row g-3">
                    <div className="form-row text-left">
                        <label forHtml="inputAddress" className="form-label">Email</label>
                        <input type="text" className="form-control" id="inputAddress" placeholder="Email" />
                    </div>
                    <div className='d-flex'>
                        <select style={{ width: "5rem" }} className="form-select me-3" aria-label="Default select example">
                            <option selected>+91</option>
                            <option value="+91">+91</option>
                            <option value="+61">+61</option>
                            <option value="+1">+1</option>
                        </select>
                        <input type="text" className="form-control" id="inputCity" />
                    </div>
                    <div className="form-check p-3 border-top border-bottom">
                        <input className="border-danger form-check-input danger m-1" type="checkbox" value="" onClick={() => {
                            this.showGstinDiv();
                        }} />
                        <label className="form-check-label" forHtml="flexCheckChecked">
                            <small>I have GSTN number(optional) ?</small>
                        </label>
                        {this.props.showGstinDiv &&
                            <>
                                <div className="row">
                                    <div className="col p-1">
                                        <label className="form-label"><small>GSTIN</small></label>
                                        <input type="text" className=" form-control" placeholder="GSTIN" aria-label="First name" />
                                    </div>
                                    <div className="col p-1">
                                        <label className="form-label"><small>Business Name</small></label>
                                        <input type="text" className=" form-control" placeholder="Business Name" aria-label="Last name" />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col p-1">
                                        <label className="form-label"><small>Business Name</small></label>
                                        <input type="text" className=" form-control" placeholder="Business Address" aria-label="First name" />
                                    </div>
                                    <div className="col p-1">
                                        <label className="form-label"><small>Business Email</small></label>
                                        <input type="text" className=" form-control" placeholder="Business Email" aria-label="Last name" />
                                    </div>
                                </div>
                            </>
                        }
                    </div>
                    <div className="form-check p-3 border-top border-bottom">
                        <input className="border-danger form-check-input danger m-1" type="checkbox" value="" />
                        <label className="form-check-label" forHtml="flexCheckChecked">
                            <small> <i className="fa-brands fa-whatsapp"></i> Subscribe to whatsapp messages.</small>
                        </label>
                    </div>
                </form>
            </>
        )
    }
}
