import React, { Component } from 'react'
import city from '../../images/city.png'
import exchange from '../../images/exchange.png'
import './SearchBar.css'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';


class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            field1: "from",
            field2: "to",
            from: "",
            to: "",
            date: new Date().toISOString().slice(0, 10),
            allowSearch: false
        };
    }
    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value
        },()=>{
            this.props.addFromTo({ from: this.state.from, to: this.state.to });
            localStorage.setItem('from',this.state.from);
            localStorage.setItem('to',this.state.to);
            if(this.state.from.length && this.state.to.length){
                this.setState({
                    allowSearch: true
                })
            }
            else{
                this.setState({
                    allowSearch: false
                })
            }
        })
        
    }
    handleSubmit = (event) => {
        event.preventDefault();

        if (!this.state.from.length && !this.state.to.length) {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    field1: prevState.field1 + " error-field",
                    field2: prevState.field2 + " error-field"
                }
            })
        }
        else if (!this.state.from.length) {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    field2: "to",
                    field1: prevState.field1 + " error-field"
                }
            })
        }
        else if (!this.state.to.length) {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    field1: "from",
                    field2: prevState.field2 + " error-field"
                }
            })
        }
        
        else {
            this.setState({
                field1: "from",
                field2: "to",
            })
        }
    }
    render() {
        return (
            <div className="search-div">
                <form onSubmit={(event) => {
                    this.handleSubmit(event)
                }}>

                    <div className="search-bar">
                        <div className={this.state.field1}>
                            <img className='city' src={city} alt="city" />
                            <input type="text" name="from" onChange={(event) => {
                                this.handleChange(event);
                            }} />
                        </div>
                        <div className="exchange">
                            <img src={exchange} alt="exchange" />
                        </div>
                        <div className={this.state.field2}>
                            <img className='city' src={city} alt="city" />
                            <input type="text" name="to" onChange={(event) => {
                                this.handleChange(event);
                            }} />
                        </div>
                        <div className="date">
                            <input type="date" min={this.state.date} defaultValue={this.state.date} name="date" />
                        </div>
                        <div className="search-bus">
                            {
                                this.state.allowSearch ?
                                    <Link to='/buses'>
                                        <button >Search Buses</button>
                                    </Link> :
                                    <button >Search Buses</button>
                            }
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addFromTo: (journeyLoaction) => dispatch({
            type: 'INITIAL',
            payload: journeyLoaction
        })
    }
}
export default connect(null, mapDispatchToProps)(SearchBar);