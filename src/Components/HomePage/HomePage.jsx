import React, { Component } from 'react'

import './HomePage.css'
import SearchBar from '../SearchBar/SearchBar'
import FirstSave from '../FirstSave/FirstSave'
import HomePageOffer from '../HomePageOffer/HomePageOffer'
import ConvinientOnTheGo from '../ConvinientOnTheGo/ConvinientOnTheGo'
import Promise from '../Promise/Promise'
import Award from '../Award/Award'
import GlobalPresence from '../GlobalParesence/GlobalPresence'
import GrowingNumber from '../GrowingNumber/GrowingNumber'
import TopSection from '../TopSection/TopSection'

export default class HomePage extends Component {
    render() {
        return (
            <div className='home-page'>
                <SearchBar />
                <FirstSave />   
                <HomePageOffer />
                <ConvinientOnTheGo />
                <Promise />
                <Award />
                <GlobalPresence />
                <GrowingNumber />
                <TopSection />
            </div>
        )
    }
}
