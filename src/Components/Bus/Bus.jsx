import React, { Component } from 'react'
import './Bus.css'
import Seats from '../Seats/Seats';
import { connect } from 'react-redux';
import axios from 'axios';
import validator from 'validator';
import BookingSummary from '../BookingSummary/BookingSummary';
import PassengerDetails from '../PassengerDetails/PassengerDetails';
import ContactDetails from '../ContactDetails/ContactDetails';
import PassengerFooter from '../PassengerFooter/PassengerFooter';
import BusDetails from '../BusDetails/BusDetails';

class Bus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSeat: false,
            seatNumber: [],
            showBillDiv: false,
            showGstinDiv: false,
            bill: 0,
            id: '',
            booking: [],
            seatBookedForLowerPlatform: [],
            seatBookedForUpperPlatform: [],
            passengers: {},
            error: {}
        }
    }
    viewSeat = () => {
        this.setState((prevState) => {
            return {
                showSeat: !prevState.showSeat
            }
        })
    }

    showBill = (id, price, seatBookedForLowerPlatform, seatBookedForUpperPlatform, booking) => {
        this.setState({
            showBillDiv: true,
            bill: booking.length * price,
            id,
            booking,
            seatBookedForLowerPlatform,
            seatBookedForUpperPlatform,

        })
    }
    bookTicket = () => {
        let seatBookedForLowerPlatform = this.state.seatBookedForLowerPlatform.map((seat) => {
            return seat !== 0 ? 2 : 0;
        })
        let seatBookedForUpperPlatform = this.state.seatBookedForUpperPlatform.map((seat) => {
            return seat !== 0 ? 2 : 0;
        })
        axios.put(`https://redbus-api.vercel.app/api/buses/${this.state.id}`, {
            seatBookedForLowerPlatform,
            seatBookedForUpperPlatform
        }).then(function (response) {
            console.log(response);
            window.location.replace('/')
        }).catch(function (error) {
            console.log(error);
        });
    }

    showGstinDiv = () => {
        this.setState((prevState) => {
            return {
                ...prevState,
                showGstinDiv: !prevState.showGstinDiv
            }
        })
    }

    checkPassengerDetails = (event, seat) => {
        const { name, value } = event.target;
        let error = ''
        let passengersName = name.includes('name') && value
        error = (name.includes("name") && !validator.isAlpha(value)) && !value.length ? 'Enter valid name' :
            (name.includes("age") && (value > 120 || value < 0)) && !value.length ? 'Enter valid age' :
                (name.includes("city") && !validator.isAlpha(value)) && !value.length ? 'Enter valid city' :
                    (name.includes("state") && !validator.isAlpha(value)) && !value.length ? 'Enter valid city' : '';

        passengersName &&
            this.setState((prevState) => {
                return {
                    ...prevState,
                    passengers: {
                        ...prevState.passengers,
                        [seat]: passengersName
                    }
                }
            })


        this.setState((prevState) => {
            return {
                ...prevState,
                error: {
                    ...prevState.error,
                    [name]: error
                },
            }
        })
    }

    render() {
        const { id, price, seatBookedForUpperPlatform, seatBookedForLowerPlatform } = this.props.bus
        let totalSeatsAvailableLower = seatBookedForLowerPlatform.filter(Boolean);
        let totalSeatsAvailableUpper = seatBookedForUpperPlatform.filter(Boolean);
        let seatsAvailable = (42 - (totalSeatsAvailableLower.length + totalSeatsAvailableUpper.length));
        let errors = Object.values(this.state.error).filter(Boolean)
        let errorFields = Object.keys(this.state.error)
        return (
            <div className='bus-detail'>
                <div className="businfo">
                    <BusDetails busInfo={this.props.bus} seatsAvailable={seatsAvailable} />
                    <div className='booking-div'>
                        <button className='view-seat' onClick={() => {
                            this.viewSeat();
                        }}>
                            <small>View Seat</small>
                        </button>
                    </div>
                </div>
                {
                    this.state.showSeat &&
                    <div style={{ background: "#EEEDED" }} className="seats-and-bill d-flex">
                        <div className="seats" style={{ background: "#fff", width: "100%" }}>
                            <Seats
                                id={id}
                                seatBookedForUpperPlatform={seatBookedForUpperPlatform}
                                seatBookedForLowerPlatform={seatBookedForLowerPlatform}
                                price={price} showBill={this.showBill} />
                        </div>
                        <div className="bill" style={{ width: "90%", padding: "2rem" }}>
                            {this.state.booking.length ?
                                <div style={{ background: "#FFF" }} className='shadow m-1 p-2'>
                                    <div className="bill-desk border my-3">
                                        <div className="m-2 p-2 d-flex justify-content-between  align-items-baseline" style={{ alignItems: "center" }}>
                                            <small>From</small>
                                            <span style={{ textTransform: "capitalize" }}><strong>{localStorage.getItem("from")}</strong></span>
                                        </div>
                                        <div className="m-2 p-2 d-flex justify-content-between  align-items-center">
                                            <small>To</small>
                                            <span><strong style={{ textTransform: "capitalize" }}>{localStorage.getItem("to")}</strong></span>
                                        </div>
                                        <div className="m-2 p-2 d-flex justify-content-between  align-items-center">
                                            <small>Seats</small>
                                            <span><strong>{this.state.booking.toString()}</strong></span>
                                        </div>
                                        <div className="m-2 p-2 d-flex justify-content-between  align-items-center">
                                            <span>Price</span>
                                            <small><strong className='text-success'>₹ {this.state.bill.toLocaleString()}</strong></small>
                                        </div>
                                    </div>
                                    <div className='data-bs-scroll'>
                                        <button className="w-100 btn btn-danger" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">Proceed to book</button>
                                        <div style={{ width: "35rem", overflow: "scroll", scrollbarWidth: "none" }} className="offcanvas offcanvas-end" tabIndex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                                            <h2 className='text-center m-1'>Passenger Details</h2>
                                            <div className="offcanvas-header d-flex flex-column border">
                                                <p className='w-100 d-flex flex-start px-4'><i className="m-1 fa-regular fa-user"></i> Passenger Information</p>
                                                {(this.state.booking.toString())
                                                    .split(',')
                                                    .map((seat, index) => {
                                                        return <PassengerDetails
                                                            error={this.state.error}
                                                            checkPassengerDetails={this.checkPassengerDetails}
                                                            seat={seat} index={index} />
                                                    })}
                                                <ContactDetails showGstinDiv={this.state.showGstinDiv} />
                                                <PassengerFooter bill={this.state.bill} errorFields={errorFields} errors={errors} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                :
                                <div className='m-1 p-1'>
                                    <div className='d-flex'>
                                        <div className="m-1 available" style={{ height: "1rem", width: "1rem", border: "1px solid black" }}></div>
                                        <span>available seats</span>
                                    </div>
                                    <div className='d-flex'>
                                        <div className="m-1 unavailable" style={{ background: "#CBCBCB", height: "1rem", width: "1rem", border: "1px solid black" }}></div>
                                        <span>unavailable seats</span>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                }
                <BookingSummary passengers={this.state.passengers} bookTicket={this.bookTicket} />
            </div >
        )
    }
}
function mapStateToProps(state) {
    return {
        from: state.bus.from,
        to: state.bus.to,
    }
}
export default connect(mapStateToProps)(Bus);