import React, { Component } from 'react'

export default class BookingSummary extends Component {
    handleClick=()=>{
        this.props.bookTicket();
    }
    
    render() {
        return (
            <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="staticBackdropLabel">Ticket Booked</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <h1 className='text-center'>Booking Summary</h1>
                            {Object.entries(this.props.passengers).map(([seat, passengerName]) => {
                                return <>
                                    <div className='d-flex justify-content-between p-3 border-top border-bottom'>
                                        <h3><strong>{seat}</strong></h3>
                                        <h3 style={{ textTransform: "capitalize" }}><strong>{passengerName}</strong></h3>
                                    </div>
                                </>
                            })}
                        </div>
                        <div className="modal-footer text-center">
                            <button className='w-100 btn btn-danger' onClick={() => {
                                this.handleClick();
                            }}>Check out</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
