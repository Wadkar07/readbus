import React, { Component } from 'react'

import offer1 from '../../images/homepage-offer1.png'
import offer2 from '../../images/homepage-offer2.png'
import './HomePageOffer.css'

export default class HomePageOffer extends Component {
    render() {
        return (
            <div className="home-page-offer">
                <div className="offer1">
                    <small>Save upto Rs 300 on Ap and TS route</small>
                    <img src={offer1} alt="" />
                    <small>Use Code SUPERHIT</small>
                </div>
                <div className="offer2">
                    <small>Save upto Rs 200 on Delhi and North routes</small>
                    <img src={offer2} alt="" />
                    <small>Use code RB200</small>
                </div>
            </div>
        )
    }
}
