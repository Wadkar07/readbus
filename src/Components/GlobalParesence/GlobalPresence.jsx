import React, { Component } from 'react'
import './GlobalPresence.css'
import Colombia from '../../images/colombia.png'
import India from '../../images/India.png'
import Indonesia from '../../images/Indonasia.png'
import Malaysia from '../../images/Malaysia.png'
import Peru from '../../images/Peru.png'
import Singapore from '../../images/Singapore.png'

export default class GlobalPresence extends Component {
    render() {
        return (
            <div className='global-presence'>
                <h2>OUR GLOBAL PRESENCE</h2>
                <div className="countries">
                    <div>
                        <img src={Colombia} alt="Colombia" />
                        <p>Colombia</p>
                    </div>
                    <div>
                        <img src={India} alt="India" />
                        <p>India</p>
                    </div>
                    <div>
                        <img src={Indonesia} alt="Indonesia" />
                        <p>Indonesia</p>
                    </div>
                    <div>
                        <img src={Malaysia} alt="Malaysia" />
                        <p>Malaysia</p>
                    </div>
                    <div>
                        <img src={Peru} alt="Peru" />
                        <p>Peru</p>
                    </div>
                    <div>
                        <img src={Singapore} alt="Singapore" />
                        <p>Singapore</p>
                    </div>
                </div>
            </div>
        )
    }
}
