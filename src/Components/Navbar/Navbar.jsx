import React, { Component } from 'react'
import logo from '../../images/white-red-bus-logo.png'
import './Navbar.css'
import { Link } from 'react-router-dom';

class Navbar extends Component {
    render() {
        return (
            <div className='navbar'>
                <div className='nav-left'>
                    <Link to='/'>
                        <img src={logo} alt="redbus" />
                    </Link>
                    <p className='bus-ticket'>BUS TICKETS</p>
                    <p className='new-red-site'>rYde <sup>NEW</sup> </p>
                    <p className='new-red-site'>redRail <sup>NEW</sup> </p>
                </div>
                <div className="nav-right">
                    <p>Help</p>
                    <div className="dropdown ">
                        <button className="btn btn-secondary dropdown-toggle d-flex align-items-center border-0" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <p>Manage Booking</p>
                        </button>
                        <ul className="dropdown-menu dropdown-menu-end">
                            <li><a className="dropdown-item disabled" href="/#">Bus Tickets</a></li>
                            <li><a className="dropdown-item" href="/#">Cancel</a></li>
                            <li><a className="dropdown-item" href="/#">Change Travel Date</a></li>
                            <li><a className="dropdown-item" href="/#">Show My Ticket</a></li>
                            <li><a className="dropdown-item" href="/#">Email/SMS</a></li>
                        </ul>
                    </div>

                    <div className="dropdown ">
                        <button className="btn btn-secondary dropdown-toggle d-flex align-items-center border-0" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <p><i className="fa-solid fa-user"></i> </p>
                        </button>
                        <ul className="dropdown-menu dropdown-menu-end">
                            <li><a className="dropdown-item" href="/#">My Trip</a></li>
                            <li><a className="dropdown-item" href="/#">Wallets/Card</a></li>
                            <li><a className="dropdown-item" href="/#">My Profile</a></li>
                            <li><a className="dropdown-item" href="/#">Wallet</a></li>
                            <li><a className="dropdown-item" href="/#">Sign Out</a></li>
                            <li><a className="dropdown-item" href="/#">Sign Out From All Devices</a></li>
                        </ul>
                    </div>
                </div>
            </div >
        )
    }
}

export default Navbar;
