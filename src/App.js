import './App.css';
import Navbar from './Components/Navbar/Navbar';
import Footer from './Components/Footer/Footer';
import React from 'react';
import HomePage from './Components/HomePage/HomePage';
import { Route, Routes } from 'react-router-dom';
import SearchResult from './Components/SearchResult/SearchResult';

class App extends React.Component {
  render() {
    return (
      <div className="App">

        <Navbar />
        <Routes>
          <Route path='/' element ={<HomePage />} />
          <Route path= '/buses' element={<SearchResult />}/>          
        </Routes>
        <Footer />
      </div>
    );
  }
}

export default App;